using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Attach to recttransform to follow cursor
    /// </summary>
    /// <remarks>
    /// Requires a CanvasScaler attached to parent
    /// </remarks>
    [RequireComponent(typeof(RectTransform))]
    public class CanvasCursor : MonoBehaviour
    {
        private Vector2 _referenceResolution = Vector2.zero;

        private void Awake()
        {
            var canvasScaler = GetComponentInParent<CanvasScaler>();

            Assert.IsTrue(canvasScaler != null);

            if (canvasScaler != null) _referenceResolution = canvasScaler.referenceResolution;
        }

        private void Update()
        {
            GetComponent<RectTransform>().anchoredPosition =
                new Vector2(Input.mousePosition.x * _referenceResolution.x / Screen.width,
                            Input.mousePosition.y * _referenceResolution.y / Screen.height);
        }
    }
}
