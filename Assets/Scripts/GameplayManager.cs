using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Registers to tile events and handles success and failure
    /// </summary>   
    public class GameplayManager : MonoBehaviour
    {                
        [SerializeField, NotNull, UsedImplicitly] private StarScoreController _starScoreController = null;
        [SerializeField, NotNull, UsedImplicitly] private SlotGenerator _slotGenerator = null;
        [SerializeField, NotNull, UsedImplicitly] private HintManager _hintManager = null;
        [SerializeField, NotNull, UsedImplicitly] private CanvasGroupController _resetButton = null;
        [SerializeField, NotNull, UsedImplicitly] private CanvasGroupController _homeButton = null;

        [SerializeField, NotNull, UsedImplicitly] private AudioSource _successSound = null;
        [SerializeField, NotNull, UsedImplicitly] private AudioSource _failureSound = null;
        
        private List<TileAnswerSlot> _answers;
        private List<Tile> _tiles;
        private List<ImageHighlightInfluencer> _influencers;

        private const float AudioDelay = 1.0f;

        private bool _resettingIsBlocked;

        public event Action OnHomeButtonPressed = delegate { }; 

        public event Action OnSuccess = delegate { };
        public event Action OnFailure = delegate { };
        public event Action<float> OnLevelComplete = delegate { };
        
        /// <summary>
        /// If GameState is not currently handling a success or failure
        /// trigger a reset
        /// </summary>
        public void RequestReset()
        {
            if (!_resettingIsBlocked) Reset(true);        
        }
                      
        /// <summary>
        /// Generates level with the provided string key
        /// </summary>
        public void BeginGameplay(string key)
        {
            _slotGenerator.GenerateSlots(key);                        
            Reset(true);

            _hintManager.GenerateHint(1.0f);
            _starScoreController.ResetScore();
            _starScoreController.Show();
            _resetButton.Show();
            _homeButton.Show();
        }

        /// <summary>
        /// Destroys and hides current level
        /// </summary>
        public void FinishGamePlay()
        {
            _answers.Clear();
            ClearAndUnregisterCollections();

            _slotGenerator.FadeAndClearSlots();
            _starScoreController.Hide();
            _resetButton.Hide();
            _homeButton.Hide();
            _hintManager.DestroyHint();
        }

        public void HomeButtonPressed()
        {
            if (!_resettingIsBlocked) OnHomeButtonPressed();
        }

        private void Awake()
        {
            _influencers = new List<ImageHighlightInfluencer>(FindObjectsOfType<ImageHighlightInfluencer>());
        }

        private void Reset(bool resetCorrect)
        {
            _answers = new List<TileAnswerSlot>(FindObjectsOfType<TileAnswerSlot>());

            ClearAndUnregisterCollections();
            InitAndRegisterCollections();
            
            var spawners = new List<TileSpawnerSlot>(FindObjectsOfType<TileSpawnerSlot>());
            spawners.Shuffle();
                      
            foreach (var tile in _tiles)
            {
                var answer = tile.GetComponentInParent<TileAnswerSlot>();

                if (answer != null)
                {
                    // release to existing slot if we want to keep correct tiles
                    if (!resetCorrect && answer.TestCondition())
                    {
                        tile.Release(answer);
                        continue;
                    }

                    var occupiedSpawners = new List<TileSpawnerSlot>();

                    foreach (var spawner in spawners)
                    {
                        if (spawner.GetComponentInChildren<Tile>())
                        {
                            occupiedSpawners.Add(spawner);
                        }
                        else
                        {
                            tile.Release(spawner);
                        }
                    }
                }
            }

            SetInfluencers(true);

            _resettingIsBlocked = false;
        }

        private void SetInfluencers(bool enable)
        {
            foreach (var influencer in _influencers)
            {
                influencer.GetComponent<Collider2D>().enabled = enable;
            }
        }

        # region Success & Failure handling
        private void DetermineSucessOrFailure()
        {
            _resettingIsBlocked = true;
            
            SetInfluencers(false);
            
            var answers = FindObjectsOfType<TileAnswerSlot>();

            foreach (var answer in answers)
            {
                if (!answer.TestCondition())
                {
                    TriggerTileFailure();
                    OnFailure();
                    return;
                }
            }

            TriggerTileSuccess();
            OnSuccess();
        }
        
        private void TriggerTileSuccess()
        {
            // access the slots backwards to trigger them forwards (based on how slot elements are inserted)
            var answers = FindObjectsOfType<TileAnswerSlot>().Reverse().ToList();
            
            for (var i = 0; i < answers.Count; ++i)
            {
                var tile = answers[i].GetComponentInChildren<Tile>();

                Assert.IsTrue(tile != null);

                if (tile == null) continue;

                tile.BeginSuccess(i);
            }
            
            _successSound.PlayDelayed(AudioDelay);
        }

        private void TriggerTileFailure()
        {
            foreach (var tile in _tiles)
            {
                tile.BeginFailure();
            }
            
            _failureSound.PlayDelayed(AudioDelay);
        }
        #endregion

        #region Tile Collection management
        private void InitAndRegisterCollections()
        {
            _tiles = new List<Tile>(FindObjectsOfType<Tile>());

            foreach (var tile in _tiles)
            {
                RegisterTile(tile);
            }
        }
        
        private void ClearAndUnregisterCollections()
        {
            if (_tiles == null) return;
            
            foreach (var tile in _tiles.Where(tile => tile))
            {
                UnregisterTile(tile);
            }

            _tiles.Clear();
        }

        private void RegisterTile(Tile tile)
        {
            Assert.IsTrue(tile != null);

            if (tile != null)
            {
                tile.OnSuccessAnimationFinished += OnTileSuccess;
                tile.OnFailureAnimationFinished += OnTileFailure;
                tile.OnTileDraggedFromSlot += OnTileLeavingSlot;
                tile.OnTileDraggedToSlot += OnTileEnteringSlot;
            }
        }

        private void UnregisterTile(Tile tile)
        {
            Assert.IsTrue(tile != null);

            if (tile != null)
            {
                tile.OnSuccessAnimationFinished -= OnTileSuccess;
                tile.OnFailureAnimationFinished -= OnTileFailure;
                tile.OnTileDraggedFromSlot -= OnTileLeavingSlot;
                tile.OnTileDraggedToSlot -= OnTileEnteringSlot;
            }
        }
        #endregion

        #region Event Handling

        private void OnTileLeavingSlot(BaseTileSlot tileSlot, Tile tile)
        {
            var answer = tileSlot as TileAnswerSlot;

            if (answer == null) return;

            _answers.Add(answer);
        }

        private void OnTileEnteringSlot(BaseTileSlot tileSlot)
        {
            var answer = tileSlot as TileAnswerSlot;

            if (answer == null) return;

            _answers.Remove(answer);

            if (_answers.Count < 1) DetermineSucessOrFailure();
        }

        private void OnTileSuccess(Tile tile)
        {
            UnregisterTile(tile);
            _tiles.Remove(tile);

            if (_tiles.Count == 0)
            {
                OnLevelComplete(_starScoreController.Score);
            }
        }

        private void OnTileFailure(Tile tile)
        {
            UnregisterTile(tile);
            _tiles.Remove(tile);

            if (_tiles.Count == 0)
            {
                _starScoreController.DecreaseScore();
                Reset(false);
            }
        }
        #endregion
    }
}
