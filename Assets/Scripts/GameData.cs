﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    /// <summary>
    /// Serializable data for each level
    /// </summary>
    [Serializable]
    public class LevelData
    {
        public string AnswerKeyword;
        public float Score;
    }

    /// <summary>
    /// Serializable data for entire game
    /// </summary>
    [Serializable]
    public class GameData
    {
        public List<LevelData> LevelData;
        public int AvailableLevels;
        public int CurrentLevel;
    }
}
