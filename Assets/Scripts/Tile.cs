using System;
using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Manages behavior of Tile interacting with TileSlots
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(AudioSource))]
    public class Tile : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private AudioClip _drag = null;        
        [SerializeField, NotNull, UsedImplicitly] private AudioClip _release = null;
        
        [SerializeField, NotNull, UsedImplicitly] private ParticleSystem _successParticles = null;
        [SerializeField, NotNull, UsedImplicitly] private ParticleSystem _failureParticles = null;
        
        private BaseTileSlot _currTileSlot;
        private BaseTileSlot _prevBaseTileSlot;
                
        public event Action<BaseTileSlot> OnTileDraggedToSlot = delegate { };
        public event Action<BaseTileSlot, Tile> OnTileDraggedFromSlot = delegate { };
        public event Action<Tile> OnSuccessAnimationFinished = delegate { };
        public event Action<Tile> OnFailureAnimationFinished = delegate { };
                                
        /// <summary>
        /// Tile will parent to and follow the provided transform
        /// </summary>
        public void Drag(RectTransform trans)
        {
            transform.SetParent(trans);

            GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

            _prevBaseTileSlot = _currTileSlot;
            
            Assert.IsTrue(_prevBaseTileSlot != null);
            OnTileDraggedFromSlot(_prevBaseTileSlot, this);

            DragEffects();

            PlayAudio(TileAudioType.Drag);
        }
             
        /// <summary>
        /// Tile will transition back to the last recorded slot
        /// </summary>
        public void Release(BaseTileSlot tileSlot = null)
        {
            if (tileSlot) _currTileSlot = tileSlot;

            transform.SetParent(_currTileSlot.transform);
            _prevBaseTileSlot = null;
            
            Assert.IsTrue(_currTileSlot != null);
            OnTileDraggedToSlot(_currTileSlot);

            ReleaseEffects();

            PlayAudio(TileAudioType.Release);
        }
       
        private void Start()
        {            
            _currTileSlot = GetComponentInParent<BaseTileSlot>();
            Assert.IsTrue(_currTileSlot);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var slot = other.GetComponent<BaseTileSlot>();

            if (slot && slot.GetComponentInChildren<Tile>() == null)
            {
                _currTileSlot = slot;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var slot = other.GetComponent<BaseTileSlot>();

            if (slot)
            {
                if (_currTileSlot == slot && _prevBaseTileSlot != null) 
                    _currTileSlot = _prevBaseTileSlot;
            }
        }

        #region Audio

        public enum TileAudioType
        {
            Drag,
            Release,
        }

        private void PlayAudio(TileAudioType audioType)
        {
            AudioClip clip = null;

            switch (audioType)
            {
                case TileAudioType.Drag:
                    clip = _drag;
                    break;
                case TileAudioType.Release:
                    clip = _release;
                    break;
                default:
                    Assert.IsTrue(false, "Unknown TileAudioType");
                    break;
            }

            GetComponent<AudioSource>().PlayOneShot(clip);
        }

        #endregion

        #region Animation
        [UsedImplicitly]
        private void UpdateAnimationPosition(Vector2 position)
        {
            GetComponent<RectTransform>().anchoredPosition = position;            
        }

        [UsedImplicitly]
        private void UpdateAnimationScale(Vector3 scale)
        {
            GetComponent<RectTransform>().localScale = scale;
        }
                   
        [UsedImplicitly]
        private void AnimationStartHelper()
        {
            GetComponent<Collider2D>().enabled = false;            
        }

        [UsedImplicitly]
        private void AnimationCompleteHelper()
        {
            GetComponent<Collider2D>().enabled = true;
        }
        
        private void DragEffects()
        {
            var hash = new Hashtable()
            {
                {"name", "drag"},
                {"time", 0.25f},
                {"amount", Vector3.one * 0.25f},
            };
            iTween.ShakeScale(gameObject, hash);
        }

        private void ReleaseEffects()
        {
            var hash = new Hashtable()
            {
                {"name", "release"},
                {"from", GetComponent<RectTransform>().anchoredPosition},
                {"to", Vector2.zero},
                {"time", 0.8f},
                {"easetype", iTween.EaseType.easeOutElastic},
                {"onstart", "AnimationStartHelper"},
                {"onupdate", "UpdateAnimationPosition"},
                {"oncomplete", "AnimationCompleteHelper"},
            };
            iTween.ValueTo(gameObject, hash);
        }
        
        /// <summary>
        /// Begins playing the success animation.
        /// The larger the priority the longer the animations take
        /// to trigger
        /// </summary>
        public void BeginSuccess(int priority)
        {                        
            const float delay = 0.15f;

            Assert.IsTrue(priority > -1);

            var priorityDelay = 1.0f + delay*priority;

            var hash = new Hashtable()
            {
                {"name", "success"},
                {"from", Vector2.one},
                {"to", Vector2.up * 30.0f},
                {"time", 0.5f},
                {"delay", priorityDelay},
                {"easetype", iTween.EaseType.easeInSine},
                {"onstart", "AnimationStartHelper"},
                {"onupdate", "UpdateAnimationPosition"},
                {"oncomplete", "SuccessEffectsReturn"},
            };
            iTween.ValueTo(gameObject, hash);

            PlaySuccessParticles(priorityDelay); 
        }

        [UsedImplicitly]
        private void SuccessEffectsReturn()
        {
            var hash = new Hashtable()
            {
                {"name", "successreturn"},
                {"from", GetComponent<RectTransform>().anchoredPosition},
                {"to", Vector2.one},
                {"time", 0.5f},
                {"easetype", iTween.EaseType.easeInSine},
                {"onupdate", "UpdateAnimationPosition"},
                {"oncomplete", "SuccessEffectsFinished"},
            };
            iTween.ValueTo(gameObject, hash);
        }

        [UsedImplicitly]
        private void SuccessEffectsFinished()
        {
            StartCoroutine(SuccessWithDelay(1.5F));            
        }

        private IEnumerator SuccessWithDelay(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            AnimationCompleteHelper();
            OnSuccessAnimationFinished(this);
        }
        
        /// <summary>
        /// Begins playing the failure animation.
        /// </summary>
        public void BeginFailure()
        {
            var scale = GetComponent<RectTransform>().localScale;

            var hash = new Hashtable()
            {
                {"name", "failure"},
                {"from", scale},
                {"to", new Vector3(scale.x, scale.y * 0.5f, scale.z)},
                {"time", 0.5f},
                {"delay", 1.0f},
                {"easetype", iTween.EaseType.easeInSine},
                {"onstart", "AnimationStartHelper"},
                {"onupdate", "UpdateAnimationScale"},
                {"oncomplete", "FailureEffectsReturn"},
            };
            iTween.ValueTo(gameObject, hash);

            PlayFailureParticles(1.5f);
        }

        [UsedImplicitly]
        private void FailureEffectsReturn()
        {
            var scale = GetComponent<RectTransform>().localScale;

            var hash = new Hashtable()
            {
                {"name", "failurereturn"},
                {"from", scale},
                {"to", new Vector3(scale.x, scale.y * 2.0f, scale.z)},
                {"time", 0.5f},
                {"easetype", iTween.EaseType.easeInSine},
                {"onupdate", "UpdateAnimationScale"},
                {"oncomplete", "FailureEffectsFinished"},
            };
            iTween.ValueTo(gameObject, hash);
        }

        [UsedImplicitly]
        private void FailureEffectsFinished()
        {
            StartCoroutine(FailureWithDelay(3.0f));
        }

        private IEnumerator FailureWithDelay(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            AnimationCompleteHelper();
            OnFailureAnimationFinished(this);
        }
        #endregion

        #region Particles
        private void PlaySuccessParticles(float delay)
        {
            StartCoroutine(PlayParticlesWithDelay(_successParticles, delay));
        }
        
        private void PlayFailureParticles(float delay)
        {
            StartCoroutine(PlayParticlesWithDelay(_failureParticles, delay));
        }

        private IEnumerator PlayParticlesWithDelay(ParticleSystem partSystem, float waitTime)
        {            
            yield return new WaitForSeconds(waitTime);
            
            partSystem.Play();

            // This seemly useless line of code helps particle system play more than just one
            // Known unity issue: https://issuetracker.unity3d.com/issues/particle-system-plays-only-once
            partSystem.startLifetime = partSystem.startLifetime;
        }
        #endregion
    }
}
