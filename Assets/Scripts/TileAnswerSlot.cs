﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Tests whether an attached Tile gameobject matches the specified letter
    /// </summary>
    /// <remarks>
    /// Must call Init before testing condition
    /// </remarks>
    public class TileAnswerSlot : BaseTileSlot
    {
        [SerializeField, UsedImplicitly] private char _letter = '\0';

        public char Letter { get { return _letter; } }
        
        public bool TestCondition()
        {
            Assert.IsTrue(_letter != '\0');
            
            var tileLetter = GetComponentInChildren<TileLetter>();
            return tileLetter != null && tileLetter.Letter == _letter;
        }

        public override void Init(char letter)
        {
            Assert.IsTrue(Char.IsLetter(letter));
            _letter = letter;
        }
    }
}
