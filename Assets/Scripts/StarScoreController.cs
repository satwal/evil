﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Set the fill of score star
    /// </summary>
    public class StarScoreController : CanvasGroupController
    {
        [SerializeField, NotNull, UsedImplicitly] private Image _fill = null;                
        [SerializeField, UsedImplicitly] private Color _colorLow = Color.clear;
        [SerializeField, UsedImplicitly] private Color _colorMed = Color.clear;
        [SerializeField, UsedImplicitly] private Color _colorHigh = Color.clear;

        private const float StartingScore = 1.0f;
        private const float DecreaseScoreStep = 0.33f;

        public float Score
        {
            get { return _fill.fillAmount; }
        }

        public void ResetScore()
        {            
            SetScoreFill(StartingScore);
        }

        /// <summary>
        /// Decreases score by predefined amount
        /// </summary>
        public void DecreaseScore()
        {
            SetScoreFill(Score - DecreaseScoreStep);
        }

        /// <summary>
        /// 0.0f - 1.0f, also sets the color of the star
        /// </summary>
        /// <param name="fill"></param>
        public void SetScoreFill(float fill)
        {
            const float animTime = 0.8f;

            var targetFill = Mathf.Clamp(fill, 0.0f, StartingScore);

            var hash = new Hashtable()
            {
                {"name", "fill"},
                {"from", _fill.fillAmount},
                {"to", targetFill},
                {"time", animTime},
                {"easetype", iTween.EaseType.easeOutElastic},                
                {"onupdate", "UpdateAnimationFill"},                
            };
            iTween.ValueTo(gameObject, hash);

            // Set color
            var targetColor = _colorLow;

            if (targetFill >= 1.0f)
            {
                targetColor = _colorHigh;
            }
            else if (targetFill >= 0.66f)
            {
                targetColor = _colorMed;
            }

            _fill.CrossFadeColor(targetColor, animTime, true, false);
        }

        [UsedImplicitly]
        private void UpdateAnimationFill(float value)
        {
            _fill.fillAmount = value;
        }
    }
}
