using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public enum InfluencerType
    {
        None,
        Input,
        Tile,
        All,
    }
    
    /// <summary>
    /// Specifies InfluencerType for attached gameobject
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class ImageHighlightInfluencer : MonoBehaviour 
    {
        [SerializeField, UsedImplicitly] private InfluencerType _influencerType = InfluencerType.None;

        public InfluencerType InfluencerType
        {
            get { return _influencerType; }
        }

        public bool IsOccupied { get; set; }
    }
}
