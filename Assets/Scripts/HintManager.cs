using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Displays animated hints.
    /// </summary>
    /// <remarks>
    /// Plays a hint only once per session
    /// If interupted, hint can be played at a later time
    /// </remarks>
    public class HintManager : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private HintController _finger = null;        
        [SerializeField, NotNull, UsedImplicitly] private CanvasGroupController _hintText = null;

        private GameObject _hint = null;

        private bool _finishedPlayingHint = false;

        /// <summary>
        /// Generates one hint at a time
        /// </summary>
        /// <param name="delay"></param>
        public void GenerateHint(float delay)
        {
            if (_finishedPlayingHint || _hint != null) return;
            
            StartCoroutine(GenerateHintWithDelay(delay));
        }

        /// <summary>
        /// Destroys current hint, if any
        /// </summary>
        public void DestroyHint()
        {
            if (_hint == null) return;

            _hintText.Hide(0.0f);
            Destroy(_hint);
        }

        private void GenerateHint()
        {            
            var spawner = new List<TileSpawnerSlot>(FindObjectsOfType<TileSpawnerSlot>());
            Assert.IsTrue(spawner.Count > 0);
            spawner.Shuffle();

            var startRect = spawner[0].GetComponent<RectTransform>();

            var targetLetter = spawner[0].GetComponentInChildren<TileLetter>();

            Assert.IsTrue(targetLetter != null);

            if (targetLetter == null) return;
            
            var answers = new List<TileAnswerSlot>(FindObjectsOfType<TileAnswerSlot>());
            Assert.IsTrue(answers.Count > 0);

            RectTransform endRect = null;
            
            foreach (var answer in answers)
            {
                if (targetLetter.Letter != answer.Letter) continue;
                endRect = answer.GetComponent<RectTransform>();
                break;
            }

            Assert.IsTrue(endRect != null);
            if (endRect == null) return;

            _hint = Instantiate(_finger.gameObject);
            _hint.transform.SetParent(startRect.transform, false);
            _hint.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            _hint.GetComponent<RectTransform>().localScale = Vector3.one;

            _hint.transform.SetParent(endRect.transform, true);

            var controller = _hint.GetComponent<HintController>();

            controller.OnHintFinished += OnHintFinished;
            controller.Animate(_hint.GetComponent<RectTransform>().anchoredPosition, Vector2.zero);
        }
        
        private IEnumerator GenerateHintWithDelay(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            _hintText.Show();
            GenerateHint();
        }

        private void OnHintFinished()
        {
            _finishedPlayingHint = true;
            _hintText.Hide();
        }
    }
}
