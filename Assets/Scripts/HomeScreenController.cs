using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Management of Home Screen visibility and buttons
    /// </summary>
    public class HomeScreenController : CanvasGroupController
    {
        [SerializeField, NotNull, UsedImplicitly] private Button _playButton = null;
        [SerializeField, NotNull, UsedImplicitly] private Button _clearButton = null;
        [SerializeField, NotNull, UsedImplicitly] private Button _levelSelectButton = null;

        public event Action OnPlayButtonPressed = delegate { };
        public event Action OnClearDataButtonPressed = delegate { };
        public event Action OnLevelSelectButtonPressed = delegate { };

        #region button callbacks
        public void PlayButtonPressed()
        {            
            OnPlayButtonPressed();
        }

        public void ClearDataButtonPressed()
        {
            OnClearDataButtonPressed();
        }

        public void LevelSelectButtonPressed()
        {
            OnLevelSelectButtonPressed();
        }
        #endregion

        protected override void OnAnimationStart()
        {
            // Left intentionally blank, not using base behavior
        }

        protected override void OnAnimationComplete()
        {
            base.OnAnimationComplete();
            _playButton.interactable = true;
            _clearButton.interactable = true;
            _levelSelectButton.interactable = true;
        }        
    }
}
