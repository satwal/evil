using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{    
    /// <summary>
    /// Tiles live in TileSlots
    /// </summary>   
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(LayoutElement))]
    public abstract class BaseTileSlot : MonoBehaviour
    {
        public abstract void Init(char letter);
    }
}
