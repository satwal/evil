using System;
using System.Collections;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Management of Level Select Screen visibility and buttons
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class LevelSelectController : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private CanvasGroupController _canvasGroupController = null;
        [SerializeField, NotNull, UsedImplicitly] private GameDataManager _gameDataManager = null;

        [SerializeField, NotNull, UsedImplicitly] private LayoutElement _levelSelectGroup = null;
        [SerializeField, NotNull, UsedImplicitly] private LevelSelectElementController _levelSelectElement = null;
        
        [SerializeField, NotNull, UsedImplicitly] private Button _prevButton = null;
        [SerializeField, NotNull, UsedImplicitly] private Button _nextButton = null;

        private const int GroupElementLimit = 8;

        private int _currGroup = 0;
        private int _numOfGroups = 0;
        private float _groupStep = 0.0f;

        private bool _generated = false;

        public event Action<int> OnLevelSelected = delegate { };
        public event Action OnHomeButtonPressed = delegate { };

        public void Show(float fadeTime = 0.5f)
        {
            if (_generated) return;            
            _generated = true;

            GenerateLevelButtons();
            _canvasGroupController.Show(fadeTime);
        }

        public void Hide(float fadeTime = 0.5f)
        {
            if (!_generated) return;
            _generated = false;

            _canvasGroupController.Hide(fadeTime);
            DestroyLevelButtons(fadeTime);
        }
        
        #region Button Generation
        private void GenerateLevelButtons()
        {
            var levelData = _gameDataManager.GameData.LevelData;

            var levelCount = 0;

            // Ensure that width starts at zero
            transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0.0f,
                transform.GetComponent<RectTransform>().sizeDelta.y);  

            while (levelCount < levelData.Count)
            {
                var group = Instantiate(_levelSelectGroup);
                group.transform.SetParent(transform, false);

                var adjustedSize = transform.GetComponent<RectTransform>().sizeDelta;
                adjustedSize.x += _levelSelectGroup.preferredWidth;
                transform.GetComponent<RectTransform>().sizeDelta = adjustedSize;

                _numOfGroups++;

                for (var j = 0; j < GroupElementLimit; ++j)
                {
                    var element = Instantiate(_levelSelectElement);
                    element.transform.SetParent(group.transform, false);

                    var controller = element.GetComponent<LevelSelectElementController>();
                    controller.Init(levelCount);

                    if (levelCount <= _gameDataManager.GameData.AvailableLevels)
                    {
                        var score = _gameDataManager.GameData.LevelData[levelCount].Score;
                        controller.SetLocked(false);
                        controller.SetScore(score);
                        controller.OnLevelSelected += OnLevelButtonPressed;
                    }

                    if (++levelCount >= levelData.Count) break;
                }
            }

            _groupStep = transform.GetComponent<RectTransform>().sizeDelta.x / (_numOfGroups);

            transform.GetComponent<RectTransform>().anchoredPosition = new Vector2((_groupStep / 2.0f) * (_numOfGroups - 1), 
                transform.GetComponent<RectTransform>().anchoredPosition.y);

            UpdateButtonStates();
        }

        private void DestroyLevelButtons(float fadeTime)
        {
            var children = transform.GetComponentsInChildren<RectTransform>();

            foreach (var child in children.Where(child => child != GetComponent<RectTransform>()))
            {
                Destroy(child.gameObject, fadeTime);
            }

            _groupStep = 0.0f;
            _currGroup = 0;
            _numOfGroups = 0;            
        }
        #endregion

        #region Button Events
        public void ShowPreviousGroup()
        {
            if (_currGroup <= 0) return;

            _currGroup--;

            DisableButtonStates();

            AnimateToPosition(new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x + _groupStep,
                                          transform.GetComponent<RectTransform>().anchoredPosition.y));
        }

        public void ShowNextGroup()
        {
            if (_currGroup >= _numOfGroups - 1) return;            

            _currGroup++;

            DisableButtonStates();

            AnimateToPosition(new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x - _groupStep,
                                          transform.GetComponent<RectTransform>().anchoredPosition.y));
        }

        public void HomeButtonPressed()
        {
            OnHomeButtonPressed();
        }

        private void OnLevelButtonPressed(int levelId)
        {
            OnLevelSelected(levelId);
        }

        private void DisableButtonStates()
        {
            _prevButton.interactable = false;
            _nextButton.interactable = false;
        }

        private void UpdateButtonStates()
        {
            _prevButton.interactable = _currGroup > 0;
            _nextButton.interactable = _currGroup < _numOfGroups - 1;
        }        
        #endregion

        #region Animations
        private void AnimateToPosition(Vector2 target)
        {            
            var hash = new Hashtable()
            {
                {"name", "group"},
                {"from", GetComponent<RectTransform>().anchoredPosition},
                {"to", target},
                {"time", 0.8f},
                {"easetype", iTween.EaseType.easeOutElastic},
                {"onstart", "DisableButtonStates"},
                {"onupdate", "UpdateGroupAnimation"},
                {"oncomplete", "UpdateButtonStates"},
            };
            iTween.ValueTo(gameObject, hash);
        }

        private void UpdateGroupAnimation(Vector2 value)
        {
            GetComponent<RectTransform>().anchoredPosition = value;
        }
        #endregion
    }
}
