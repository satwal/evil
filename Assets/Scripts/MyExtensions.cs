﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class MyExtensions
    {
        private static readonly Random Rng = new Random();
        
        /// <summary>
        /// Yates shuffle algorithm
        /// </summary>
        /// <remarks>
        /// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
        /// </remarks>
        public static void Shuffle<T>(this IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = Rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            } 
        }
    }
}
