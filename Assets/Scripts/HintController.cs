﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Manages the animation and lifetime of the hint hand
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class HintController : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private List<Image> _uiImages = null;
        
        public event Action OnHintFinished = delegate { };
              
        /// <summary>
        /// Fade in hand, move between provided positions, fade out
        /// </summary>
        public void Animate(Vector2 from, Vector2 to)
        {
            var hash = new Hashtable()
            {
                {"name", "hint"},
                {"from", from},
                {"to", to},
                {"time", 2.0f},
                {"delay", 1.0f},
                {"easetype", iTween.EaseType.easeInSine},
                {"onupdate", "UpdateHintAnimation"},
                {"oncomplete", "FinishHintAnimation"},
            };
            iTween.ValueTo(gameObject, hash);

            SetHandImagesVisible(true, 0.5f);
        }

        private void Awake()
        {
            SetHandImagesVisible(false);
        }

        [UsedImplicitly]
        private void UpdateHintAnimation(Vector2 position)
        {
            GetComponent<RectTransform>().anchoredPosition = position;
        }

        [UsedImplicitly]
        private void FinishHintAnimation()
        {
            OnHintFinished();
            SetHandImagesVisible(false, 0.5f);
            Destroy(gameObject, 1.0f);
        }

        private void SetHandImagesVisible(bool visible, float time = 0.0f)
        {
            foreach (var image in _uiImages)
            {
                image.CrossFadeAlpha(visible ? 1.0f : 0.0f, time, true);
            }
        }
    }
}
