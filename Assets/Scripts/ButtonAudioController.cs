﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Plays Attached AudioSource when attached button is clicked
    /// </summary>
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(AudioSource))]
    public class ButtonAudioController : MonoBehaviour 
    {
        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
