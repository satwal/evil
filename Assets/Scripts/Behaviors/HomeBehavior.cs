using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Behaviors
{
    /// <summary>
    /// State behavior for Home Screen
    /// </summary>
    public class HomeBehavior : GameStateBehavior
    {
        private HomeScreenController _homeScreenController = null;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            _homeScreenController = FindObjectOfType<HomeScreenController>();
            Assert.IsTrue(_homeScreenController != null);

            if (_homeScreenController == null) return;            
            _homeScreenController.OnPlayButtonPressed += OnPlayButtonPressed;
            _homeScreenController.OnClearDataButtonPressed += OnClearDataButtonPressed;
            _homeScreenController.OnLevelSelectButtonPressed += OnLevelSelectButtonPressed;
            _homeScreenController.Show();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {            
            _homeScreenController.OnPlayButtonPressed -= OnPlayButtonPressed;
            _homeScreenController.OnClearDataButtonPressed -= OnClearDataButtonPressed;
            _homeScreenController.OnLevelSelectButtonPressed -= OnLevelSelectButtonPressed;
            _homeScreenController.Hide();
        }

        #region button callbacks
        private void OnPlayButtonPressed()
        {
            Animator.SetTrigger(GameplayTriggerName);
        }

        private void OnLevelSelectButtonPressed()
        {
            Animator.SetTrigger(LevelSelectTriggerName);
        }

        private void OnClearDataButtonPressed()
        {
            GameDataManager.ClearGameData();
        }
        #endregion
    }
}
