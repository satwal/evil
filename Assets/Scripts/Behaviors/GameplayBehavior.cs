using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Behaviors
{
    /// <summary>
    /// State behavior for Gameplay
    /// </summary>
    public class GameplayBehavior : GameStateBehavior
    {
        private GameplayManager _gameplayManager = null;
        private ResultsScreenController _resultsScreenController = null;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            _resultsScreenController = FindObjectOfType<ResultsScreenController>();
            Assert.IsTrue(_resultsScreenController != null);
            
            _gameplayManager = FindObjectOfType<GameplayManager>();
            Assert.IsTrue(_gameplayManager != null);
            if (_gameplayManager == null) return;

            _gameplayManager.OnLevelComplete += OnLevelComplete;
            _gameplayManager.OnHomeButtonPressed += OnHomeButtonPressed;
            _gameplayManager.BeginGameplay(GameDataManager.CurrentAnswerKeyword);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {            
            _gameplayManager.OnLevelComplete -= OnLevelComplete;
            _gameplayManager.OnHomeButtonPressed -= OnHomeButtonPressed;
            _gameplayManager.FinishGamePlay();
        }

        private void OnLevelComplete(float score)
        {
            GameDataManager.CurrentScore = score;
            _resultsScreenController.SetStarScore(score);            
            Animator.SetTrigger(GameplayFinishedTriggerName);
        }

        private void OnHomeButtonPressed()
        {
            Animator.SetTrigger(HomeTriggerName);
        }
    }
}
