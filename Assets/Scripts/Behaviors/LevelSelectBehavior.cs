using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Behaviors
{
    /// <summary>
    /// State behavior for Level Select Screen
    /// </summary>
    public class LevelSelectBehavior : GameStateBehavior
    {
        private LevelSelectController _levelSelectController = null;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);
            
            _levelSelectController = FindObjectOfType<LevelSelectController>();
            Assert.IsTrue(_levelSelectController != null);

            if (_levelSelectController == null) return;            
            _levelSelectController.OnLevelSelected += OnLevelButtonPressed;
            _levelSelectController.OnHomeButtonPressed += OnHomeButtonPressed;
            _levelSelectController.Show();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {            
            _levelSelectController.OnLevelSelected -= OnLevelButtonPressed;
            _levelSelectController.OnHomeButtonPressed -= OnHomeButtonPressed;
            _levelSelectController.Hide();
        }

        #region button callbacks
        private void OnLevelButtonPressed(int levelId)
        {
            GameDataManager.SetNextLevel(levelId);
            Animator.SetTrigger(GameplayTriggerName);
        }

        private void OnHomeButtonPressed()
        {            
            Animator.SetTrigger(HomeTriggerName);
        }
        #endregion
    }
}
