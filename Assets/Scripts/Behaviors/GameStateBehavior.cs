using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Behaviors
{
    /// <summary>
    /// Base class for all GameStates
    /// </summary>
    public abstract class GameStateBehavior : StateMachineBehaviour
    {        
        // Animator trigger names
        public const string HomeTriggerName = "Home";
        public const string GameplayTriggerName = "Gameplay";
        public const string LevelSelectTriggerName = "LevelSelect";
        public const string GameplayFinishedTriggerName = "Results";

        public Animator Animator { get; private set; }
        public GameDataManager GameDataManager { get; private set; }
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Animator = animator;
            Assert.IsTrue(Animator != null);

            GameDataManager = FindObjectOfType<GameDataManager>();
            Assert.IsTrue(GameDataManager != null);
        }
    }
}
