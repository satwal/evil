using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Behaviors
{
    public class ResultsBehavior : GameStateBehavior
    {
        private ResultsScreenController _resultsScreenController = null;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            GameDataManager.CurrentLevelComplete();
            
            _resultsScreenController = FindObjectOfType<ResultsScreenController>();
            Assert.IsTrue(_resultsScreenController != null);

            if (_resultsScreenController == null) return;            
            _resultsScreenController.OnNextButtonPressed += OnNextButtonPressed;
            _resultsScreenController.OnRetryButtonPressed += OnRetryButtonPressed;
            _resultsScreenController.OnIntroButtonPressed += OnIntroButtonPressed;
            _resultsScreenController.Show();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {            
            _resultsScreenController.OnNextButtonPressed -= OnNextButtonPressed;
            _resultsScreenController.OnRetryButtonPressed -= OnRetryButtonPressed;
            _resultsScreenController.OnIntroButtonPressed -= OnIntroButtonPressed;
            _resultsScreenController.Hide();
        }

        #region button callbacks
        private void OnRetryButtonPressed()
        {
            Animator.SetTrigger(GameplayTriggerName);
        }

        private void OnNextButtonPressed()
        {
            Animator.SetTrigger(GameplayTriggerName);
            GameDataManager.SetNextLevel();
        }

        private void OnIntroButtonPressed()
        {
            Animator.SetTrigger(HomeTriggerName);
            GameDataManager.SetNextLevel();
        }
        #endregion
    }
}
