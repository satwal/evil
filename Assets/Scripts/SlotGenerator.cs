﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Manages the creation and destruction of tile slots
    /// </summary>
    public class SlotGenerator : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private LayoutGroup _spawnLayoutGroup = null;
        [SerializeField, NotNull, UsedImplicitly] private LayoutGroup _answerLayoutGroup = null;
        [SerializeField, NotNull, UsedImplicitly] private LayoutGroup _tileSlotRow = null;
        [SerializeField, NotNull, UsedImplicitly] private BaseTileSlot _tileSlotSpawnPrefab = null;
        [SerializeField, NotNull, UsedImplicitly] private BaseTileSlot _tileSlotAnswerPrefab = null;
        [SerializeField, NotNull, UsedImplicitly] private LayoutElement _deadSlotElementPrefab = null;
                
        private const int MaxCharLength = 13;
        private const int MaxWordLength = 6;
        private const int MaxSpawnRowLength = 6;
        private const int MaxSpawnRowCount = 2;

        /// <summary>
        /// Deletes all slots from layout with fade
        /// </summary>
        public void FadeAndClearSlots(float fadeTime = 0.5f)
        {
            FadeAndDestroyChildren(_answerLayoutGroup.transform, fadeTime);
            FadeAndDestroyChildren(_spawnLayoutGroup.transform, fadeTime);
        }

        /// <summary>
        /// Creates both spawn and answer slots based on the provided string
        /// </summary>
        /// <remarks>
        /// string cannot exceed 13 characters and each word cannot exceed 6 characters
        /// </remarks>
        public void GenerateSlots(string key)
        {
            if (key.Length > MaxCharLength)
            {
                Assert.IsTrue(false, "Max character length exceeded: " + key);
                return;
            }

            var words = key.Split();

            Assert.IsTrue(words.Length > 0, "No words: " + words);

            foreach (var word in words.Where(word => word.Length > MaxWordLength))
            {
                Assert.IsTrue(false, "Invalid word length: " + word);
                return;
            }
            
            GenerateSlotsFromWords(words, _answerLayoutGroup, _tileSlotAnswerPrefab);
            
            // string to list, shuffle, back to string
            var shuffledArr = key.Replace(" ", String.Empty).ToCharArray();
            shuffledArr.Shuffle();
            var shuffledKey = shuffledArr.Aggregate("", (current, keyChar) => current + keyChar);

            GenerateSlotsFromKey(shuffledKey, _spawnLayoutGroup, _tileSlotSpawnPrefab);
        }

        private void GenerateSlotsFromKey(string key, LayoutGroup slotGroup, BaseTileSlot slotType)
        {
            GenerateSlotsFromWords(new List<string>() { key }, slotGroup, slotType);
        }

        private void GenerateSlotsFromWords(IList<string> words, LayoutGroup slotGroup, BaseTileSlot slotType)
        {
            var currWordIndex = 0;

            var currWord = words[currWordIndex].ToList();
                        
            for (var rowCount = 0; rowCount < MaxSpawnRowCount; ++rowCount)
            {
                // Break out of loop if we're out of words
                if (currWordIndex >= words.Count) break;
                
                var row = Instantiate(_tileSlotRow);
                row.transform.SetParent(slotGroup.transform, false);

                for (var slotCount = 0; slotCount < MaxSpawnRowLength; ++slotCount)
                {
                    // Decide to generate a space on the current row for the next word,
                    // or move to the next row
                    if (currWord.Count == 0)
                    {
                        // If we're out of words, break out of loop  
                        currWordIndex++;

                        if (currWordIndex >= words.Count) break;
                        
                        currWord = words[currWordIndex].ToList();
                                                                       
                        if (currWord.Count > MaxSpawnRowLength - slotCount) break;

                        if (slotCount != 0)
                        {
                            var deadSlot = Instantiate(_deadSlotElementPrefab);
                            deadSlot.transform.SetParent(row.transform, false);
                            continue;
                        }                        
                    }

                    var letter = currWord[0];
                    currWord.RemoveAt(0);

                    var slot = Instantiate(slotType.gameObject);
                    slot.transform.SetParent(row.transform, false);

                    var spawner = slot.GetComponent<BaseTileSlot>();
                    Assert.IsTrue(spawner != null);

                    if (spawner != null) spawner.Init(letter);
                }                
            }

            Assert.IsTrue(currWord.Count == 0, "Unused characters found during tile slot generation: " + currWord);
        }
        
        private void FadeAndDestroyChildren(Component target, float fadeTime)
        {
            var children = target.GetComponentsInChildren<CanvasGroupController>();
            foreach (var child in children.Where(child => child != _spawnLayoutGroup.transform))
            {
                child.Hide(fadeTime);
                Destroy(child.gameObject, fadeTime);
            }
        }
    }
}
