using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{      
    /// <summary>
    /// Wrapper for interacting with GameData
    /// </summary>
    public class GameDataManager : MonoBehaviour
    {        
        [SerializeField, UsedImplicitly] private List<string> _answerKeywords = new List<string>();

        private const string DataKey = "Data";
        
        public string CurrentAnswerKeyword
        {
            get { return Current.AnswerKeyword; }
        }

        public float CurrentScore
        {
            get { return Current.Score; }
            set { Current.Score = value; }
        }

        public LevelData Current
        {
            get { return GameData.LevelData[GameData.CurrentLevel]; }
        }
               
        public GameData GameData { get; private set; }

        /// <summary>
        /// Notfies manager that the current level has been completed
        /// </summary>
        public void CurrentLevelComplete()
        {
            // Only unlock next level, if we just finished the latest
            if (GameData.AvailableLevels != GameData.CurrentLevel) return;

            GameData.AvailableLevels = Mathf.Clamp(GameData.AvailableLevels + 1, 0, GameData.LevelData.Count - 1);
        }

        public void SetNextLevel()
        {
            GameData.CurrentLevel = ++GameData.CurrentLevel % GameData.LevelData.Count;
        }

        /// <summary>
        /// Sets the next level based on provided level index
        /// </summary>
        public void SetNextLevel(int i)
        {
            Assert.IsTrue(i >= 0 && i <= GameData.AvailableLevels);
            GameData.CurrentLevel = i;
        }
        
        /// <summary>
        /// Clears out GameData
        /// </summary>
        public void ClearGameData()
        {
            PlayerPrefs.SetString(DataKey, "");
            LoadGameData();
        }
        
        private void Awake()
        {
            LoadGameData();
        }

        private void OnDestroy()
        {
            SaveGameData();
        }

        private void SaveGameData()
        {
            var jsonStr = JsonConvert.SerializeObject(GameData);
            PlayerPrefs.SetString(DataKey, jsonStr);
        }

        private void LoadGameData()
        {
            var file = PlayerPrefs.GetString(DataKey, String.Empty);

            // build gamedata from scratch if none is present
            if (file == String.Empty)
            {
                GameData = new GameData { LevelData = new List<LevelData>() };

                foreach (var key in _answerKeywords)
                {
                    GameData.LevelData.Add(new LevelData { AnswerKeyword = key, Score = 0 });
                }

                GameData.AvailableLevels = 0;
                GameData.CurrentLevel = 0;
            }
            else
            {
                GameData = JsonConvert.DeserializeObject<GameData>(file);
            }

            Assert.IsTrue(GameData != null);
        }        
    }
}
