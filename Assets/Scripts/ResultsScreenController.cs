﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Management of Results Screen visibility and buttons
    /// </summary>
    public class ResultsScreenController : CanvasGroupController
    {
        [SerializeField, NotNull, UsedImplicitly] private StarScoreController _starScoreController = null;
        
        [SerializeField, NotNull, UsedImplicitly] private Button _retryButton = null;
        [SerializeField, NotNull, UsedImplicitly] private Button _nextButton = null;
        [SerializeField, NotNull, UsedImplicitly] private Button _homeButton = null;

        [UsedImplicitly] public event Action OnRetryButtonPressed = delegate { };
        [UsedImplicitly] public event Action OnNextButtonPressed = delegate { };
        [UsedImplicitly] public event Action OnIntroButtonPressed = delegate { };

        #region button callbacks
        public void SetStarScore(float score)
        {
            _starScoreController.SetScoreFill(score);
        }

        public void RetryButtonPressed()
        {
            OnRetryButtonPressed();
        }

        public void NextButtonPressed()
        {
            OnNextButtonPressed();
        }

        public void IntroButtonPressed()
        {
            OnIntroButtonPressed();
        }
        #endregion

        protected override void OnAnimationStart()
        {
            base.OnAnimationStart();
            _retryButton.interactable = false;
            _nextButton.interactable = false;
            _homeButton.interactable = false;
        }

        protected override void OnAnimationComplete()
        {
            _retryButton.interactable = true;
            _nextButton.interactable = true;
            _homeButton.interactable = true;
        }
    }
}
