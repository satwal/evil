using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Highlights attached Image based on collisions with ImageHighlightInfluencer
    /// </summary>
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Collider2D))]
    public class ImageHighlighter : MonoBehaviour 
    {
        [SerializeField, UsedImplicitly] private Color _highlightColor = Color.white;

        [SerializeField, UsedImplicitly] private InfluencerType _allowInfluencerType = InfluencerType.None;

        private Color _startingColor;

        private const float CollisionExpiry = 0.05f;
        private float _timeSinceLastCollision = 0.0f;

        private ImageHighlightInfluencer _currInfluencer = null;
              
        public void SetHighlightEnable(bool enable)
        {
            GetComponent<Image>().color = enable ? _highlightColor : _startingColor;             
        }

        private void Awake()
        {
            _startingColor = GetComponent<Image>().color;
        }

        private void FixedUpdate()
        {
            // Unity optimizes rigidbodies to sleep when inactive
            // We don't want this, if we're constantly polling for a triggerstay
            if (GetComponent<Rigidbody2D>() != null)
                GetComponent<Rigidbody2D>().WakeUp();
            
            _timeSinceLastCollision += Time.fixedDeltaTime;

            if (_timeSinceLastCollision > CollisionExpiry)
            {                
                _timeSinceLastCollision = 0.0f;
                SetHighlightEnable(false);
                if (_currInfluencer != null) _currInfluencer.IsOccupied = false;
            }
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            TestCollision(other.collider, true);
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            TestCollision(other, true);
        }
        
        private void TestCollision(Collider2D col, bool onCollisionHightlight)
        {
            if (_allowInfluencerType == InfluencerType.None) return;
            
            var influencer = col.GetComponent<ImageHighlightInfluencer>();

            if (influencer != null && !influencer.IsOccupied
                    && (influencer.InfluencerType == InfluencerType.All 
                        || influencer.InfluencerType == _allowInfluencerType))
            {
                _currInfluencer = influencer;
                _currInfluencer.IsOccupied = true;
                SetHighlightEnable(onCollisionHightlight);
                _timeSinceLastCollision = 0.0f;
            }
        }
    }
}
