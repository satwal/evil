using UnityEngine;

namespace Assets.Scripts
{    
    /// <summary>
    /// Handles dragging and releasing of Tiles
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(RectTransform))]
    public class TileDragger : MonoBehaviour
    {
        private Tile _currentElement = null;
        
        private void Update ()
        {
            if (_currentElement != null && Input.GetMouseButtonUp(0))
            {
                _currentElement.Release();
                _currentElement = null;
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            HandleDragging(other);
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            HandleDragging(other);
        }

        private void HandleDragging(Collision2D other)
        {
            if (!Input.GetMouseButton(0) || _currentElement != null) return;

            if (other.collider.GetComponent<Tile>() != null)
            {
                _currentElement = other.collider.GetComponent<Tile>();
                _currentElement.Drag(GetComponent<RectTransform>());
            }
        }
    }
}
