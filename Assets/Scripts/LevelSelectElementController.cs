using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    /// <summary>
    /// Sets locked/star state of levelselect element, handles button callbacks
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class LevelSelectElementController : MonoBehaviour
    {
        [SerializeField, NotNull, UsedImplicitly] private StarScoreController _starScoreController = null;
        [SerializeField, NotNull, UsedImplicitly] private Image _lockImage = null;

        public event Action<int> OnLevelSelected = delegate { }; 

        private int _levelIndex = -1;
        
        /// <summary>
        /// Must call after instantiating component
        /// </summary>
        /// <param name="levelIndex"></param>
        public void Init(int levelIndex)
        {
            _levelIndex = levelIndex;
        }
        
        /// <summary>
        /// Show a lock or a score star
        /// </summary>
        public void SetLocked(bool locked)
        {
            if (locked)
            {
                GetComponent<Button>().interactable = false;
                _lockImage.CrossFadeAlpha(1.0f, 0.0f, true);
                _starScoreController.Hide(0.0f);
            }
            else
            {
                GetComponent<Button>().interactable = true;
                _lockImage.CrossFadeAlpha(0.0f, 0.0f, true);
                _starScoreController.Show(0.0f);
            }

        }

        public void SetScore(float value)
        {            
            _starScoreController.SetScoreFill(value);
        }

        public void OnButtonPress()
        {
            OnLevelSelected(_levelIndex);
        }
    }
}
