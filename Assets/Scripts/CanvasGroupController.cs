﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Manages Canvas Group alpha and fade animations
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class CanvasGroupController : MonoBehaviour
    {
        [SerializeField, UsedImplicitly] private bool _interactable = false;

        public void Show(float fadeTime = 0.5f)
        {            
            SetAnimation(true, fadeTime);
        }

        public void Hide(float fadeTime = 0.5f)
        {                      
            SetAnimation(false, fadeTime);
        }

        [UsedImplicitly]
        protected virtual void OnAnimationStart()
        {
            GetComponent<CanvasGroup>().interactable = false;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        [UsedImplicitly]
        protected virtual void OnAnimationComplete()
        {
            var interactable = GetComponent<CanvasGroup>().alpha >= 1.0f && _interactable;
            GetComponent<CanvasGroup>().interactable = interactable;
            GetComponent<CanvasGroup>().blocksRaycasts = interactable;
        }
                               
        [UsedImplicitly]
        private void UpdateFade(float value)
        {
            GetComponent<CanvasGroup>().alpha = value;
        }

        private void SetAnimation(bool show, float fadeTime)
        {
            var hash = new Hashtable()
            {
                {"name", "animate"},
                {"from", GetComponent<CanvasGroup>().alpha},
                {"to", show ? 1.0f : 0.0f},
                {"time", fadeTime},
                {"easetype", iTween.EaseType.linear},
                {"onstart", "OnAnimationStart"},
                {"onupdate", "UpdateFade"},
                {"oncomplete", "OnAnimationComplete"},
            };
            iTween.ValueTo(gameObject, hash);
        }
    }
}
