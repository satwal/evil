using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    /// <summary>
    /// Creates a tile gameobject and attaches as child
    /// </summary>
    public class TileSpawnerSlot : BaseTileSlot
    {
        [SerializeField, UsedImplicitly, NotNull] private Tile _tilePrefab = null;

        public override void Init(char letter)
        {
            var tile = Instantiate(_tilePrefab.gameObject);
            tile.transform.SetParent(transform, false);

            var tileLetter = tile.GetComponentInChildren<TileLetter>();
            Assert.IsTrue(tileLetter != null);

            if (tileLetter != null)
            {
                tileLetter.Letter = letter;
            }
        }

    }
}
