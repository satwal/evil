﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Assets.Scripts
{    
    /// <summary>
    /// Stores and displays the letter for a tile
    /// </summary>
    /// <remarks>
    /// It is assumed that a TileLetter is attached to a child gameobject of Tile
    /// </remarks>
    [RequireComponent(typeof(Text))]
    public class TileLetter : MonoBehaviour
    {
        public char Letter
        {
            get
            {
                var charArr = GetComponent<Text>().text.ToCharArray();
                Assert.IsTrue(charArr.Length < 2);

                return charArr.Length > 0 ? charArr[0] : '\0';
            }

            set
            {
                Assert.IsTrue(Char.IsLetter(value));
                GetComponent<Text>().text = "" + Char.ToLower(value);
            }
        }
    }
}
