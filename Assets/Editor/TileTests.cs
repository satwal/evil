﻿using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using NUnit.Framework;

namespace Assets.Editor
{
    [TestFixture]
    [Category("Tile Tests")]
    internal class TileTests : MonoBehaviour
    {
        private const string Answerkey = "evilempire";
        
        /// <summary>
        /// Tests whether the number of tiles and number of tile answers are equal
        /// </summary>
        [Test]
        public void TileAndAnswersAreEqual()
        {
            var tiles = FindObjectsOfType<Tile>();
            var tileAnswerslots = FindObjectsOfType<TileAnswerSlot>();

            Assert.IsTrue(tiles.Length == tileAnswerslots.Length);
        }

        /// <summary>
        /// Tests whether all tile letters match the answer
        /// </summary>
        [Test]
        public void AllTileLetters()
        {
            var answer = Answerkey;
            
            var letters = new List<TileLetter>(FindObjectsOfType<TileLetter>());

            foreach (var letter in letters)
            {
                var letterIndex = answer.IndexOf(letter.Letter);

                Assert.IsTrue(letterIndex > -1);
                answer = answer.Remove(letterIndex, 1);
            }

            Assert.IsTrue(answer.Length == 0);
        }

        /// <summary>
        /// Tests whether all tile answers match the answer
        /// </summary>
        [Test]
        public void AllTileAnswers()
        {
            var answerKey = Answerkey;

            var answers = new List<TileAnswerSlot>(FindObjectsOfType<TileAnswerSlot>());

            foreach (var answer in answers)
            {
                var letterIndex = answerKey.IndexOf(answer.Letter);

                Assert.IsTrue(letterIndex > -1);
                answerKey = answerKey.Remove(letterIndex, 1);
            }

            Assert.IsTrue(answerKey.Length == 0);
        }
        
        /// <summary>
        /// Snaps Tiles to their correct condition and tests whether GameplayManager acknowledges.
        /// Visual test...
        /// </summary>
        /// <remarks>
        /// Don't expect this to work if you already placed an incorrect tile in an TileAnswerSlot
        /// This is intended as a quick and dirty test
        /// </remarks>
        [Test]
        public void SuccessTest()
        {
            var tiles = new List<Tile>(FindObjectsOfType<Tile>());
            var tileAnswerSlots = FindObjectsOfType<TileAnswerSlot>();

            foreach (var answerSlot in tileAnswerSlots)
            {
                var removeTileIndex = -1;

                for (var i = 0; i < tiles.Count; ++i)
                {
                    if (tiles[i].GetComponentInChildren<TileLetter>().Letter == answerSlot.Letter)
                    {
                        tiles[i].Release(answerSlot);

                        removeTileIndex = i;

                        break;
                    }
                }

                if (removeTileIndex > -1)
                {
                    tiles.RemoveAt(removeTileIndex);
                }
            }
        }

        /// <summary>
        /// Snaps Tiles to answers where at least one is incorrect. Should trigger failure.
        /// Visual test...
        /// </summary>
        /// <remarks>
        /// Don't expect this to pass if you already placed a tile in an TileAnswerSlot
        /// This is intended as a quick and dirty test
        /// </remarks>
        [Test]
        public void FailureTest()
        {
            var tiles = new List<Tile>(FindObjectsOfType<Tile>());
            var tileAnswerSlots = new List<TileAnswerSlot>(FindObjectsOfType<TileAnswerSlot>());
                        
            tileAnswerSlots.Shuffle();

            var foundIncorrect = false;

            foreach (var tileAnswerSlot in tileAnswerSlots)
            {
                var removeTileIndex = -1;

                for (var i = 0; i < tiles.Count; ++i)
                {
                    if (!foundIncorrect)
                    {
                        if (tiles[i].GetComponentInChildren<TileLetter>().Letter != tileAnswerSlot.Letter)
                        {
                            foundIncorrect = true;
                            tiles[i].Release(tileAnswerSlot);
                            removeTileIndex = i;
                            break;
                        }
                        continue;
                    }

                    tiles[i].Release(tileAnswerSlot);
                    removeTileIndex = i;
                    break;
                }

                if (removeTileIndex > -1)
                {
                    tiles.RemoveAt(removeTileIndex);
                }
            }
        }
    }
}
